package ru.reboot.dto;

import java.time.LocalDateTime;

public class PriceInfo {

    private String itemId;
    private Double price;
    private String lastAccessTime;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public static class Builder {

        private final PriceInfo newPriceInfo;

        public Builder() {
            this.newPriceInfo = new PriceInfo();
        }

        public Builder setId(String id) {
            newPriceInfo.itemId = id;
            return this;
        }

        public Builder setPrice(Double price) {
            newPriceInfo.price = price;
            return this;
        }

        public Builder setLastAccessTime(String lastAccessTime) {
            newPriceInfo.lastAccessTime = lastAccessTime;
            return this;
        }

        public PriceInfo build() {
            return newPriceInfo;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "PriceInfo{" +
                "itemId='" + itemId + '\'' +
                ", price=" + price +
                ", lastAccessTime=" + lastAccessTime +
                '}';
    }
}
