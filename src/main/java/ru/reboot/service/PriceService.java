package ru.reboot.service;

import ru.reboot.dto.PriceInfo;

import java.util.Collection;
import java.util.List;

public interface PriceService {

    /**
     * Get item price
     *
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if item id is null or empty
     * @throws ru.reboot.error.BusinessLogicException with code PRICE_NOT_FOUND if item price not found
     */
    PriceInfo getPrice(String itemId);

    /**
     * Find all prices
     */
    List<PriceInfo> findAllPrices();

    /**
     * Set item price
     *
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if price is null
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if price contains bad data
     */
    PriceInfo savePrice(PriceInfo price);

    /**
     * Set all price
     *
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if prices is null
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if prices contains bad data
     */
    Collection<PriceInfo> saveAllPrices(Collection<PriceInfo> prices);

    /**
     * Delete price
     *
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if item id is null or empty
     * @throws ru.reboot.error.BusinessLogicException with code PRICE_NOT_FOUND if item price not found
     */
    void deletePrice(String itemId);
}
