package ru.reboot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.dto.PriceChangedEvent;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Component
public class PriceServiceImpl implements PriceService {

    private static final Logger logger = LogManager.getLogger(PriceServiceImpl.class);

    private ObjectMapper mapper;
    private KafkaTemplate<String, String> kafkaTemplate;
    private PriceRepository priceRepository;

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setKafkaTemplate(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Autowired
    public void setPriceRepository(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public PriceInfo getPrice(String itemId) {
        logger.info("Method .getPrice itemId={}", itemId);
        PriceEntity entity;
        PriceInfo info;
        try {
            validateItemId(itemId, ".getPrice");
            entity = priceRepository.findPrice(itemId);

            if (Objects.isNull(entity)) {
                throw new BusinessLogicException("Price not found", ErrorCode.PRICE_NOT_FOUND);
            }

            info = convertPriceEntityToPriceInfo(entity);
        } catch (Exception ex) {
            logger.error("Failed to .getPrice error={}", ex.toString(), ex);
            throw ex;
        }

        logger.info("Method .getPrice itemId={} completed result={}", itemId, info);
        return info;
    }

    /**
     * Find all prices
     *
     * @return List<PriceInfo>
     * @throws BusinessLogicException with code DATABASE_ERROR
     */
    @Override
    public List<PriceInfo> findAllPrices() throws BusinessLogicException {

        logger.info("Method .findAllPrices");

        List<PriceInfo> priceInfoList = new ArrayList<>();
        try {
            priceRepository.findAllPrices()
                    .forEach(priceEntity -> priceInfoList.add(convertPriceEntityToPriceInfo(priceEntity)));
        } catch (BusinessLogicException e) {
            logger.error("Failed to .findAllPrices error={}", e.toString(), e);
            throw e;
        }

        logger.info("Method .findAllPrices completed result={}", priceInfoList);
        return priceInfoList;
    }

    /**
     * @param price Save {@link PriceEntity} price
     * @throws {@link BusinessLogicException} with code ILLEGAL_ARGUMENT if price is null
     * @throws {@link BusinessLogicException} with code ILLEGAL_ARGUMENT if price contains bad data
     */
    @Override
    public PriceInfo savePrice(PriceInfo price) {
        logger.info("method .savePrice price={}", price);

        try {
            if (Objects.isNull(price) || price.getPrice() <= 0) {
                throw new BusinessLogicException("Price is null or less than zero", ErrorCode.ILLEGAL_ARGUMENT);
            }
            PriceInfo priceInfo = convertPriceEntityToPriceInfo(priceRepository
                    .savePrice(convertPriceInfoToPriceEntity(price)));
            sendMessage(new PriceChangedEvent(PriceChangedEvent.Type.UPDATE, priceInfo));
            logger.info("Method .savePrice completed, price={}", price);
            return priceInfo;
        } catch (Exception ex) {

            logger.error("Failed to .savePrice error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Set all price
     *
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if prices is null
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if prices contains bad data
     */
    @Override
    public Collection<PriceInfo> saveAllPrices(Collection<PriceInfo> prices) {

        logger.info("Method .saveAllPrice prices={} ", prices);

        Collection<PriceEntity> priceEntities = new ArrayList<>();
        Collection<PriceInfo> priceInfos = new ArrayList<>();

        try {

            if (Objects.isNull(prices)) {
                throw new BusinessLogicException("Prices is null or less than zero ", ErrorCode.ILLEGAL_ARGUMENT);
            }

            for (PriceInfo price : prices) {
                if (price.getPrice() <= 0) {
                    throw new BusinessLogicException("Prices contains bad data ", ErrorCode.ILLEGAL_ARGUMENT);
                }
                priceEntities.add(convertPriceInfoToPriceEntity(price));
            }

            Collection<PriceEntity> backPriceEntities = priceRepository.saveAllPrices(priceEntities);

            for (PriceEntity price : backPriceEntities) {
                PriceInfo priceInfo = convertPriceEntityToPriceInfo(price);
                priceInfos.add(priceInfo);
                sendMessage(new PriceChangedEvent(PriceChangedEvent.Type.UPDATE, priceInfo));
            }

            logger.info("Method .saveAllPrice completed prices={} result={}", prices, priceInfos);
            return priceInfos;

        } catch (Exception ex) {
            logger.error("Failed to .savePrice error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Удалить цену на конретный товар из базы данных
     *
     * @param itemId
     */
    @Override
    public void deletePrice(String itemId) {
        logger.info("method .deletePrice itemId={}", itemId);
        try {
            if (Objects.isNull(itemId) || Objects.equals(itemId, "")) {
                throw new BusinessLogicException("ERROR: Item id is null or empty", ErrorCode.ILLEGAL_ARGUMENT);
            }
            PriceEntity deletedPrice = priceRepository.findPrice(itemId);
            if (Objects.isNull(deletedPrice)) {
                throw new BusinessLogicException("ERROR: Price not found", ErrorCode.PRICE_NOT_FOUND);
            }
            PriceInfo price = getPrice(itemId);
            priceRepository.deletePrice(itemId);
            sendMessage(new PriceChangedEvent(PriceChangedEvent.Type.DELETE, price));
            logger.info("Method .deletePrice completed itemId={}", itemId);
        } catch (Exception ex) {
            logger.error("Failed to .deletePrice error={}", ex.toString(), ex);
            throw ex;
        }
    }

    private PriceInfo convertPriceEntityToPriceInfo(PriceEntity entity) {

        return PriceInfo.builder()
                .setId(entity.getItemId())
                .setPrice(entity.getPrice())
                .setLastAccessTime(entity.getLastAccessTime())
                .build();
    }

    private PriceEntity convertPriceInfoToPriceEntity(PriceInfo info) {
        PriceEntity priceEntity = new PriceEntity();
        priceEntity.setItemId(info.getItemId());
        priceEntity.setPrice(info.getPrice());
        priceEntity.setLastAccessTime(LocalDateTime.now().toString());
        return priceEntity;
    }

    private void validateItemId(String itemId, String methodName) {
        if (Objects.isNull(itemId) || itemId.equals("")) {
            throw new BusinessLogicException("Item id is null or empty", ErrorCode.ILLEGAL_ARGUMENT);
        }
    }

    public void sendMessage(PriceChangedEvent priceChangedEvent) {

        try {
            logger.info(String.format("#### -> Price Service message -> %s", mapper.writeValueAsString(priceChangedEvent)));
            kafkaTemplate.send(PriceChangedEvent.TOPIC, mapper.writeValueAsString(priceChangedEvent));
            logger.info(">> Send: TOPIC={}, {}, eventType={}", PriceChangedEvent.TOPIC, priceChangedEvent, priceChangedEvent.getEventType());
        } catch (Exception e) {
            logger.error("Failed to .sendMessage error={}, eventType={}", e.toString(), priceChangedEvent.getEventType(), e);
        }
    }
}
