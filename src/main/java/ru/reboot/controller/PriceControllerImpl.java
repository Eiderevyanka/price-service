package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dao.PriceRepositoryImpl;
import ru.reboot.dto.PriceInfo;
import ru.reboot.service.PriceService;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Price controller.
 */
@RestController
@RequestMapping(path = "price")
public class PriceControllerImpl implements PriceController {

    private static final Logger logger = LogManager.getLogger(PriceRepositoryImpl.class);

    private PriceService priceService;

    @Autowired
    public void setPriceService(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "PriceController " + new Date();
    }

    /**
     * Удалить цену на конретный товар из базы данных
     *
     * @param itemId
     */
    @DeleteMapping
    public void deletePrice(@RequestParam("itemId") String itemId) {
        priceService.deletePrice(itemId);
    }

    @Override
    @GetMapping
    public PriceInfo getPrice(@RequestParam("itemId") String itemId) {
        return priceService.getPrice(itemId);
    }

    @Override
    @GetMapping("/all")
    public List<PriceInfo> findAllPrices() {
        return priceService.findAllPrices();
    }

    /**
     * Set item price
     *
     * @param price
     * @return {@link PriceInfo} Saved price
     */
    @Override
    @PutMapping
    public PriceInfo savePrice(@RequestBody PriceInfo price) {
        return priceService.savePrice(price);
    }

    @Override
    @PutMapping("/all")
    public Collection<PriceInfo> saveAllPrices(@RequestBody Collection<PriceInfo> prices) {
        return priceService.saveAllPrices(prices);
    }

}
