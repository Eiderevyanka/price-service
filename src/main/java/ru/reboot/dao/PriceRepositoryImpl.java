package ru.reboot.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static ru.reboot.error.ErrorCode.DATABASE_ERROR;

/**
 * Price repository.
 */
@Component
public class PriceRepositoryImpl implements PriceRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PriceEntity findPrice(String itemId) {
        List<PriceEntity> prices;
        try (Session session = sessionFactory.openSession()) {
            Query<PriceEntity> query = session.createQuery("from PriceEntity where itemId = :itemId", PriceEntity.class);
            query.setParameter("itemId", itemId);
            prices = query.list();
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCode.DATABASE_ERROR);
        }

        if (prices.size() > 1) {
            throw new BusinessLogicException("Database error", ErrorCode.DATABASE_ERROR);
        }

        return prices.isEmpty() ? null : prices.get(0);
    }

    /**
     * Find all prices
     *
     * @return List<PriceEntity>
     * @throws BusinessLogicException with code DATABASE_ERROR
     */
    @Override
    public List<PriceEntity> findAllPrices() throws BusinessLogicException {

        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from PriceEntity", PriceEntity.class).list();
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), DATABASE_ERROR);
        }
    }

    /**
     * @param price Create {@link PriceEntity} price or update if already exists
     * @throws {@link BusinessLogicException} with code DATABASE_ERROR if {@link Exception}
     */
    @Override
    public PriceEntity savePrice(PriceEntity price) {

        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(price);
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new BusinessLogicException(e.getMessage(), ErrorCode.DATABASE_ERROR);
        }

        return findPrice(price.getItemId());
    }

    /**
     * Set all price
     *
     * @throws {@link BusinessLogicException} with code DATABASE_ERROR if {@link Exception}
     */
    @Override
    public Collection<PriceEntity> saveAllPrices(Collection<PriceEntity> prices) {

        Collection<String> savedIds = new ArrayList<>();
        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            for (PriceEntity price : prices) {
                session.saveOrUpdate(price);
                savedIds.add(price.getItemId());
            }

            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new BusinessLogicException(e.getMessage(), ErrorCode.DATABASE_ERROR);
        }

        return savedIds.stream()
                .map(this::findPrice)
                .collect(Collectors.toList());
    }

    /**
     * Удалить цену на конретный товар из базы данных
     *
     * @param itemId
     */
    @Override
    public void deletePrice(String itemId) {
        PriceEntity deletedPrice = new PriceEntity.Builder().setId(itemId).build();
        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(deletedPrice);
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new BusinessLogicException(e.getMessage(), ErrorCode.DATABASE_ERROR);
        }
    }
}
