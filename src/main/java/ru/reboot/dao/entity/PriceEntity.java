package ru.reboot.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "price")
public class PriceEntity {

    @Id
    @Column(name = "item_id")
    private String itemId;

    @Column(name = "price")
    private Double price;

    @Column(name = "last_access_time")
    private String lastAccessTime;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public static class Builder {

        private final PriceEntity newPriceEntity;

        public Builder() {
            this.newPriceEntity = new PriceEntity();
        }

        public Builder setId(String id) {
            newPriceEntity.itemId = id;
            return this;
        }

        public Builder setPrice(Double price) {
            newPriceEntity.price = price;
            return this;
        }

        public Builder setLastAccessTime(String lastAccessTime) {
            newPriceEntity.lastAccessTime = lastAccessTime;
            return this;
        }

        public PriceEntity build() {
            return newPriceEntity;
        }
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public String toString() {
        return "PriceEntity{" +
                "itemId='" + itemId + '\'' +
                ", price=" + price +
                ", lastAccessTime=" + lastAccessTime +
                '}';
    }
}
