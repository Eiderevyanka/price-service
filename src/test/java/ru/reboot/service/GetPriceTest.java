package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;

public class GetPriceTest {

    private final String ITEM_ID = "01";
    private final String TIME = LocalDateTime.now().toString();
    private final Double PRICE = 10.0;

    private final PriceEntity PRISE_ENTITY = PriceEntity.builder()
            .setId(ITEM_ID)
            .setPrice(PRICE)
            .setLastAccessTime(TIME)
            .build();

    @Test
    public void getPricePositive(){
        PriceRepository repository = Mockito.mock(PriceRepository.class);
        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repository);

        when(repository.findPrice(ITEM_ID)).thenReturn(PRISE_ENTITY);
        PriceInfo priceInfo = service.getPrice(ITEM_ID);

        Assert.assertEquals(ITEM_ID, priceInfo.getItemId());
        Assert.assertEquals(PRICE, priceInfo.getPrice());
        Assert.assertEquals(TIME, priceInfo.getLastAccessTime());
    }

    @Test
    public void getPriceNotFound(){
        PriceRepository repository = Mockito.mock(PriceRepository.class);
        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repository);

        when(repository.findPrice(ITEM_ID)).thenReturn(null);
        try{
            service.getPrice(ITEM_ID);
            Assert.fail();
        }catch (BusinessLogicException e){
            Assert.assertEquals(e.getCode(), ErrorCode.PRICE_NOT_FOUND);
        }
    }

    @Test
    public void getPriceItemIdIsEmpty(){
        PriceRepository repository = Mockito.mock(PriceRepository.class);
        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repository);

        try{
            service.getPrice("");
            Assert.fail();
        }catch (BusinessLogicException e){
            Assert.assertEquals(e.getCode(), ErrorCode.ILLEGAL_ARGUMENT);
        }
    }
}
