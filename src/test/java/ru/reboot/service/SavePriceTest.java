package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

public class SavePriceTest {

    @Test
    public void savePriceTest_priceIsNull() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        try {
            service.savePrice(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals("ILLEGAL_ARGUMENT", e.getCode());
        }
        Mockito.verify(repositoryMock, Mockito.times(0)).savePrice(Mockito.any());
    }

    @Test
    public void savePriceTest_priceIsLessZero() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);
        PriceInfo infoMock = Mockito.mock(PriceInfo.class);

        Mockito.when(infoMock.getPrice()).thenReturn(-10d);

        try {
            service.savePrice(infoMock);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals("ILLEGAL_ARGUMENT", e.getCode());
        }

        Mockito.verify(repositoryMock, Mockito.times(0)).savePrice(Mockito.any());
    }

    @Test
    public void savePriceTest_DATABASE_ERROR() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);
        PriceInfo infoMock = Mockito.mock(PriceInfo.class);

        Mockito.when(infoMock.getPrice()).thenReturn(10d);
        Mockito.when(repositoryMock.savePrice(Mockito.any()))
                .thenThrow(new BusinessLogicException("", ErrorCode.DATABASE_ERROR));

        try {
            service.savePrice(infoMock);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals("DATABASE_ERROR", e.getCode());
        }

        Mockito.verify(repositoryMock, Mockito.times(1)).savePrice(Mockito.any());
    }

    @Test
    public void savePriceTest_positive() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        PriceEntity priceEntity = PriceEntity.builder()
                .setId("100")
                .setPrice(101.2)
                .build();

        Mockito.when(repositoryMock.savePrice(Mockito.any())).thenReturn(priceEntity);

        PriceInfo priceInfo = PriceInfo.builder()
                .setId("100")
                .setPrice(101.2)
                .build();

        PriceInfo actualPrice = service.savePrice(priceInfo);

        Assert.assertEquals(priceInfo.getItemId(), actualPrice.getItemId());
        Assert.assertEquals(priceInfo.getPrice(), actualPrice.getPrice());

        Mockito.verify(repositoryMock, Mockito.times(1)).savePrice(Mockito.any());
    }
}
