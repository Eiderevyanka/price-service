package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SaveAllPricesTest {

    private final PriceEntity priceEntity1 = PriceEntity.builder().setId("1").setPrice(20.0).setLastAccessTime(LocalDateTime.now().toString()).build();
    private final PriceEntity priceEntity2 = PriceEntity.builder().setId("2").setPrice(10.0).setLastAccessTime(LocalDateTime.now().toString()).build();
    private final PriceEntity priceEntity3 = PriceEntity.builder().setId("1").setPrice(-5.0).setLastAccessTime(LocalDateTime.now().toString()).build();
    private final PriceInfo priceInfo = PriceInfo.builder().setId("1").setPrice(-20.0).setLastAccessTime(LocalDateTime.now().toString()).build();

    private final Collection<PriceEntity> priceEntities = new ArrayList<>();
    private final Collection<PriceInfo> priceInfos = new ArrayList<>();

    {


        priceEntities.add(priceEntity1);
        priceEntities.add(priceEntity2);

        for (PriceEntity price : priceEntities) {
            PriceInfo priceInfo = PriceInfo.builder().setId(price.getItemId())
                    .setPrice(price.getPrice()).setLastAccessTime(price.getLastAccessTime()).build();
            priceInfos.add(priceInfo);
        }

    }

    @Test
    public void saveAllPricesPositive_Test() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
        Mockito.when(repositoryMock.saveAllPrices(Mockito.anyCollection())).thenReturn(priceEntities);

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        List<PriceInfo> result = (ArrayList<PriceInfo>) service.saveAllPrices(priceInfos);
        List<PriceInfo> priceInfosList = (ArrayList<PriceInfo>) (priceInfos);

        Assert.assertEquals(result.get(0).getItemId(), priceInfosList.get(0).getItemId());
        Assert.assertEquals(result.get(0).getPrice(), priceInfosList.get(0).getPrice());
        Assert.assertEquals(result.get(1).getItemId(), priceInfosList.get(1).getItemId());
        Assert.assertEquals(result.get(1).getPrice(), priceInfosList.get(1).getPrice());

        Mockito.verify(repositoryMock, Mockito.times(1)).saveAllPrices(Mockito.anyCollection());

    }

    @Test
    public void saveAllPrices_WhenPrisesIsNull_Test() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
        Mockito.when(repositoryMock.saveAllPrices(null)).thenThrow(new BusinessLogicException("", ErrorCode.ILLEGAL_ARGUMENT));

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        try {
            service.saveAllPrices(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
            Mockito.verify((repositoryMock), Mockito.times(0)).saveAllPrices(Mockito.anyCollection());
        }
    }

    @Test
    public void saveAllPrices_WhenPrisesContainsBadData_Test() {

        Collection<PriceInfo> negativeValueInfo = new ArrayList<>();
        negativeValueInfo.add(priceInfo);

        Collection<PriceEntity> negativeValueEntity = new ArrayList<>();
        negativeValueEntity.add(priceEntity3);

        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
        Mockito.when(repositoryMock.saveAllPrices(negativeValueEntity)).thenThrow(new BusinessLogicException("", ErrorCode.ILLEGAL_ARGUMENT));

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        try {
            service.saveAllPrices(negativeValueInfo);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
            Mockito.verify((repositoryMock), Mockito.times(0)).saveAllPrices(Mockito.anyCollection());
        }
    }
}