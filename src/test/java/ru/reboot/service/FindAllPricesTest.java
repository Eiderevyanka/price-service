package ru.reboot.service;

import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static ru.reboot.error.ErrorCode.DATABASE_ERROR;

public class FindAllPricesTest {

    private final PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);
    private final PriceServiceImpl service = new PriceServiceImpl();
    private final PriceEntity priceEntity1;
    private final PriceEntity priceEntity2;

    {
        service.setPriceRepository(repositoryMock);

        priceEntity1 = PriceEntity.builder()
                .setId("1")
                .setPrice(11.0)
                .setLastAccessTime(LocalDateTime.MIN.toString())
                .build();

        priceEntity2 = PriceEntity.builder()
                .setId("2")
                .setPrice(22.0)
                .setLastAccessTime(LocalDateTime.MAX.toString())
                .build();
    }

    @Test
    public void findAllPricesPositive() {

        Mockito.when(repositoryMock.findAllPrices()).thenReturn(Arrays.asList(priceEntity1, priceEntity2));
        List<PriceInfo> prices = service.findAllPrices();

        assertEquals(prices.get(0).getItemId(), priceEntity1.getItemId());
        assertEquals((prices.get(0).getPrice()), priceEntity1.getPrice());
        assertEquals((prices.get(0).getLastAccessTime()), priceEntity1.getLastAccessTime());
        assertEquals(prices.get(1).getItemId(), priceEntity2.getItemId());
        assertEquals((prices.get(1).getPrice()), priceEntity2.getPrice());
        assertEquals((prices.get(1).getLastAccessTime()), priceEntity2.getLastAccessTime());

        Mockito.verify(repositoryMock).findAllPrices();
    }

    @Test
    public void findAllPricesPositiveEmptyList() {

        Mockito.when(repositoryMock.findAllPrices()).thenReturn(Collections.emptyList());
        assertEquals(service.findAllPrices(), Collections.emptyList());
        Mockito.verify(repositoryMock).findAllPrices();
    }

    @Test
    public void findAllPricesDatabaseError() {

        Mockito.when(repositoryMock.findAllPrices()).thenThrow(new BusinessLogicException("", DATABASE_ERROR));
        try {
            service.findAllPrices();
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "DATABASE_ERROR");
            Mockito.verify(repositoryMock).findAllPrices();
        }
    }
}