package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dao.entity.PriceEntity;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import static org.mockito.Mockito.doThrow;

public class DeletePrice_Test {

    /**
     * Checking whether the method deletePrice() is accessed
     */
    @Test
    public void deletePrice_Test_Positive() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        Mockito.when(repositoryMock.findPrice("ItemId")).thenReturn(new PriceEntity());

        service.deletePrice("ItemId");

        Mockito.verify(repositoryMock, Mockito.times(1)).deletePrice("ItemId");
    }

    /**
     * Checking for null value itemId
     * Method deletePrice() should throw a BusinessLogicException with CODE = ILLEGAL_ARGUMENT
     */
    @Test
    public void deletePrice_testException_whenItemIdIsNull() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        try {
            service.deletePrice(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCode.ILLEGAL_ARGUMENT);
        }

        Mockito.verify(repositoryMock, Mockito.times(0)).deletePrice(null);
    }

    /**
     * Checking method if itemId doesn't exist
     * Method deletePrice() should throw a BusinessLogicException with CODE = PRICE_NOT_FOUND
     */
    @Test
    public void deletePrice_testException_whenItemIdIsNotFound() {
        PriceRepository repositoryMock = Mockito.mock(PriceRepository.class);

        PriceServiceImpl service = new PriceServiceImpl();
        service.setPriceRepository(repositoryMock);

        doThrow(new BusinessLogicException("Price Not Found", ErrorCode.PRICE_NOT_FOUND)).when(repositoryMock).deletePrice("ItemId");

        try {
            service.deletePrice("ItemId");
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCode.PRICE_NOT_FOUND);
        }

        Mockito.verify(repositoryMock, Mockito.times(0)).deletePrice(null);
    }
}


